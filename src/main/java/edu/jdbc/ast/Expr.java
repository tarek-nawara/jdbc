package edu.jdbc.ast;

import edu.jdbc.lexical.Token;

import java.util.List;

public abstract class Expr {
    interface Visitor<R> {
        R visitLogicalExpr(Expr.Logical logical);

        R visitBinaryExpr(Expr.Binary binary);

        R visitUnaryExpr(Expr.Unary unary);

        R visitColumnExpr(Expr.Column column);

        R visitAlwaysTrueExpr(Expr.AlwaysTrue alwaysTrue);

        R visitGetExpr(Expr.Get get);

        R visitLiteralExpr(Expr.Literal literal);

        R visitSelectSubQueryExpr(Expr.SelectSubQuery selectSubQuery);

        R visitGroupingExpr(Expr.Grouping grouping);

        R visitListLiteralExpr(Expr.ListLiteral listLiteral);

        R visitInExpr(Expr.In in);

        R visitNotInExpr(Expr.NotIn notIn);
    }

    public static class Logical extends Expr {
        Expr left;
        Token operator;
        Expr right;


        public Logical(Expr left, Token operator, Expr right) {
            this.left = left;
            this.operator = operator;
            this.right = right;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitLogicalExpr(this);
        }
    }

    public static class Binary extends Expr {
        Expr left;
        Token operator;
        Expr right;

        public Binary(Expr left, Token operator, Expr right) {
            this.left = left;
            this.operator = operator;
            this.right = right;
        }


        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitBinaryExpr(this);
        }
    }

    public static class Unary extends Expr {
        Token operator;
        Expr right;

        public Unary(Token operator, Expr right) {
            this.operator = operator;
            this.right = right;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitUnaryExpr(this);
        }
    }

    public static class Column extends Expr {
        Token name;

        public Column(Token name) {
            this.name = name;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitColumnExpr(this);
        }
    }

    public static class Get extends Expr {
        Expr expr;
        Token name;
        public Get(Expr expr, Token name) {
            this.expr = expr;
            this.name = name;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitGetExpr(this);
        }
    }

    public static class Literal extends Expr {
        Object value;

        public Literal(Object value) {
            this.value = value;
        }


        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitLiteralExpr(this);
        }
    }

    public static class AlwaysTrue extends Expr {

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitAlwaysTrueExpr(this);
        }
    }

    public static class SelectSubQuery extends Expr {
        public abstract static class Projection {
        }

        public static class All extends Projection {
        }

        public static class Columns extends Projection {
            public List<Expr> columns;

            public Columns(List<Expr> columns) {
                this.columns = columns;
            }
        }

        Projection projection;
        Token table;
        Expr clause;

        public SelectSubQuery(Projection projection, Token table, Expr clause) {
            this.projection = projection;
            this.table = table;
            this.clause = clause;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitSelectSubQueryExpr(this);
        }
    }

    public static class Grouping extends Expr {
        Expr expr;
        public Grouping(Expr expr) {
            this.expr = expr;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitGroupingExpr(this);
        }
    }

    public static class ListLiteral extends Expr {
        public List<Expr> items;
        public ListLiteral(List<Expr> items) {
            this.items = items;
        }


        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitListLiteralExpr(this);
        }
    }

    public static class In extends Expr {
        public Expr left;
        public Token operator;
        public Expr right;

        public In(Expr left, Token operator, Expr right) {
            this.left = left;
            this.operator = operator;
            this.right = right;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitInExpr(this);
        }
    }

    abstract <R> R accept(Visitor<R> visitor);

    public static class NotIn extends Expr {
        public Expr expr;
        public Token operator;
        public Expr right;

        public NotIn(Expr expr, Token operator, Expr right) {
            this.expr = expr;
            this.operator = operator;
            this.right = right;
        }


        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitNotInExpr(this);
        }
    }
}
