package edu.jdbc.ast;

public abstract class DBType {
    public static class Int extends DBType {
    }

    public static class Varchar extends DBType {
        int length;

        public Varchar(int length) {
            this.length = length;
        }
    }
}
