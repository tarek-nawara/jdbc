package edu.jdbc.ast;

import edu.jdbc.lexical.Token;

import java.util.List;
import java.util.Map;

public abstract class Stmt {
    interface Visitor<R> {
        R visitCreateDBStatement(Stmt.CreateDB createDB);
        R visitCreateTableStatement(Stmt.CreateTable createTable);
        R visitSelectStatement(Stmt.Select select);
        R visitInsertStatement(Stmt.Insert insert);
        R visitDeleteStatement(Stmt.Delete delete);
        R visitDropTableStatement(Stmt.DropTable dropTable);
        R visitDropDBStatement(Stmt.DropDB dropDB);
        R visitUpdateStatement(Stmt.Update update);
    }

    public static class CreateDB extends Stmt {
        Token dbname;

        public CreateDB(Token dbname) {
            this.dbname = dbname;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitCreateDBStatement(this);
        }
    }

    public static class CreateTable extends Stmt {
        public static class Column {
            Token name;
            DBType type;

            public Column(Token name, DBType type) {
                this.name = name;
                this.type = type;
            }
        }

        Token table;
        List<Column> columns;

        public CreateTable(Token table, List<Column> columns) {
            this.table = table;
            this.columns = columns;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitCreateTableStatement(this);
        }
    }

    public static class Select extends Stmt {
        public abstract static class Projection {
        }

        public static class All extends Projection {
        }

        public static class Columns extends Projection {
            public List<Expr> columns;

            public Columns(List<Expr> columns) {
                this.columns = columns;
            }
        }

        Projection projection;
        Token table;
        Expr clause;

        public Select(Projection projection, Token table, Expr clause) {
            this.projection = projection;
            this.table = table;
            this.clause = clause;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitSelectStatement(this);
        }
    }

    public static class Insert extends Stmt {
        public static abstract class Order {
        }

        public static class SameOrder extends Order {
        }

        public static class CustomOrder extends Order {
            List<Token> customOrder;

            public CustomOrder(List<Token> customOrder) {
                this.customOrder = customOrder;
            }
        }

        Token table;
        Order order;
        List<Expr> values;

        public Insert(Token table, Order order, List<Expr> values) {
            this.table = table;
            this.order = order;
            this.values = values;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitInsertStatement(this);
        }
    }

    public static class Delete extends Stmt {
        Token table;
        Expr clause;

        public Delete(Token table, Expr clause) {
            this.table = table;
            this.clause = clause;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitDeleteStatement(this);
        }
    }

    public static class DropTable extends Stmt {
        Token table;

        public DropTable(Token table) {
            this.table = table;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitDropTableStatement(this);
        }
    }

    public static class DropDB extends Stmt {
        Token dbname;

        public DropDB(Token dbname) {
            this.dbname = dbname;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitDropDBStatement(this);
        }
    }

    public static class Update extends Stmt {
        Token table;
        Map<Token, Expr> newValues;
        Expr clause;

        public Update(Token table, Map<Token, Expr> newValues, Expr clause) {
            this.table = table;
            this.newValues = newValues;
            this.clause = clause;
        }

        @Override
        <R> R accept(Visitor<R> visitor) {
            return visitor.visitUpdateStatement(this);
        }
    }

    abstract <R> R accept(Visitor<R> visitor);
}
