package edu.jdbc.parser;

import edu.jdbc.ast.DBType;
import edu.jdbc.ast.Expr;
import edu.jdbc.ast.Stmt;
import edu.jdbc.lexical.Token;
import edu.jdbc.lexical.TokenType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static edu.jdbc.lexical.TokenType.*;

public class SqlParser {
    static class SqlParseError extends RuntimeException {
        public SqlParseError(Token token, String msg) {
            super(String.format("At line %d, %s", token.line(), msg));
        }
    }

    private final List<Token> tokens;

    private int current;

    public SqlParser(List<Token> tokens) {
        this.tokens = tokens;
    }

    public Stmt parse() {
        return statement();
    }

    // ---------- Statements ------ //
    private Stmt statement() {
        if (match(CREATE)) return create();
        if (match(SELECT)) return select();
        if (match(INSERT)) return insert();
        if (match(UPDATE)) return update();
        if (match(DELETE)) return delete();
        if (match(DROP))   return drop();

        throw error("Unsupported statement");
    }

    private Stmt create() {
        if (match(DATABASE)) {
            var dbname = consume(IDENTIFIER, "Expect database name.");
            consume(SEMICOLON, "Expect ';' after create db statement");
            return new Stmt.CreateDB(dbname);
        } else if (match(TABLE)) {
            var table = consume(IDENTIFIER, "Expect table name.");
            consume(LEFT_PAREN, "Expect '(' after table name.");

            List<Stmt.CreateTable.Column> columns = new ArrayList<>();
            do {
                var name = consume(IDENTIFIER, "Expect column name");
                var type = dbtype();
                columns.add(new Stmt.CreateTable.Column(name, type));
            } while (match(COMMA));

            consume(RIGHT_PAREN, "Expect ')' after column definitions.");
            consume(SEMICOLON, "Expect ';' after create table statement");

            return new Stmt.CreateTable(table, columns);
        }

        throw error("Unsupported create statement.");
    }

    private Stmt select() {
        Stmt.Select.Projection projection;
        if (match(STAR)) {
            projection = new Stmt.Select.All();
        } else {
            List<Expr> columns = new ArrayList<>();
            do {
                var column = expression();
                columns.add(column);
            } while (match(COMMA));
            projection = new Stmt.Select.Columns(columns);
        }

        consume(FROM, "Expect 'from' in select statement");
        var table = consume(IDENTIFIER, "Expect table name after 'from'");
        Expr clause = new Expr.AlwaysTrue();
        if (match(WHERE)) {
            clause = expression();
        }

        consume(SEMICOLON, "Expect ';' after select statement");

        return new Stmt.Select(projection, table, clause);
    }

    private Stmt insert() {
        consume(INTO, "Expect 'into' after insert.");
        var table = consume(IDENTIFIER, "Expect table name after 'into' in insert statement.");

        Stmt.Insert.Order order = new Stmt.Insert.SameOrder();
        if (match(LEFT_PAREN)) {
            ArrayList<Token> columns = new ArrayList<>();
            do {
                var column = consume(IDENTIFIER, "Expect column name");
                columns.add(column);
            } while (match(COMMA));
            consume(RIGHT_PAREN, "Expect ')' after custom insertion order.");
            order = new Stmt.Insert.CustomOrder(columns);
        }

        consume(VALUES, "Expect 'values' in insert statement.");
        consume(LEFT_PAREN, "Expect '(' after values.");

        List<Expr> values = new ArrayList<>();
        do {
            values.add(expression());
        } while(match(COMMA));

        consume(SEMICOLON, "Expect ';' after insert statement.");

        return new Stmt.Insert(table, order, values);
    }

    private Stmt update() {
        var table = consume(IDENTIFIER, "Expect table name.");
        consume(SET, "Expect 'set' after table name");

        Map<Token, Expr> newValues = new HashMap<>();
        do {
            var column = consume(IDENTIFIER, "Expect column name.");
            consume(EQUAL, "Expect '=' after column name.");
            var value = expression();
            newValues.put(column, value);
        } while (match(COMMA));

        Expr clause = new Expr.AlwaysTrue();
        if (match(WHERE)) {
            clause = expression();
        }

        consume(SEMICOLON, "Expect ';' after update statement.");

        return new Stmt.Update(table, newValues, clause);
    }

    private Stmt delete() {
        consume(FROM, "Expected 'from' keyword");
        var table = consume(IDENTIFIER, "Expected table name");
        Expr expr = new Expr.AlwaysTrue();
        if (match(WHERE)) {
            expr = expression();
        }

        consume(SEMICOLON, "Expect ';' after delete");
        return new Stmt.Delete(table, expr);
    }

    private Stmt drop() {
        if (match(TABLE)) {
            consume(IDENTIFIER, "Table name is expected");
            return new Stmt.DropTable(prev());
        } else if (match(DATABASE)) {
            consume(IDENTIFIER, "Database name is expected");
            return new Stmt.DropDB(prev());
        }

        throw error("Unrecognized drop statement");
    }

    // ------------ Expressions ------- //
    private Expr expression() {
        return or();
    }

    private Expr or() {
        Expr expr = and();
        while (match(OR)) {
            var operator = prev();
            Expr right = and();
            expr = new Expr.Logical(expr, operator, right);
        }

        return expr;
    }

    private Expr and() {
        Expr expr = equality();
        while (match(AND)) {
            var operator = prev();
            Expr right = equality();
            expr = new Expr.Logical(expr, operator, right);
        }

        return expr;
    }

    private Expr equality() {
        Expr expr = comparison();
        while (match(EQUAL, NOT_EQUAL)) {
            var operator = prev();
            Expr right = comparison();
            expr = new Expr.Binary(expr, operator, right);
        }

        return expr;
    }

    private Expr comparison() {
        Expr expr = term();
        while (match(GREATER, GREATER_EQUAL, LESS, LESS_EQUAL)) {
            var operator = prev();
            Expr right = term();
            expr = new Expr.Binary(expr, operator, right);
        }

        return expr;
    }

    private Expr term() {
        Expr expr = factor();
        while (match(PLUS, MINUS)) {
            var operator = prev();
            Expr right = factor();
            expr = new Expr.Binary(expr, operator, right);
        }

        return expr;
    }

    private Expr factor() {
        Expr expr = unary();
        while (match(STAR, SLASH)) {
            var operator = prev();
            Expr right = unary();
            expr = new Expr.Binary(expr, operator, right);
        }

        return expr;
    }

    private Expr unary() {
        if (match(NOT, MINUS)) {
            var operator = prev();
            var right = unary();
            return new Expr.Unary(operator, right);
        }

        return in();
    }

    private Expr in() {
        Expr expr = call();
        if (match(NOT)) {
            var operator = consume(IN, "Must have an 'in' in 'not-in' expression");
            var right = subQuery();
            expr = new Expr.NotIn(expr, operator, right);
        } else if (match(IN)) {
            var operator = prev();
            var right = subQuery();
            expr = new Expr.In(expr, operator, right);
        }

        return expr;
    }

    private Expr call() {
        Expr expr = primary();
        while (match(DOT)) {
            var name = consume(IDENTIFIER, "Expect column name");
            expr = new Expr.Get(expr, name);
        }

        return expr;
    }

    private Expr primary() {
        if (match(TRUE))                    return new Expr.Literal(true);
        if (match(FALSE))                   return new Expr.Literal(false);
        if (match(IDENTIFIER))              return new Expr.Column(prev());
        if (match(SELECT))                  return selectSubQuery();
        if (match(NUMBER) || match(STRING)) return new Expr.Literal(prev().literal());

        if (match(LEFT_PAREN)) {
            var expr = expression();
            expr = new Expr.Grouping(expr);
            consume(RIGHT_PAREN, "Expect ')' after expression.");
            return expr;
        }

        throw error("Expected expression.");
    }

    private Expr subQuery() {
        consume(LEFT_PAREN, "Expect '(' before subquery");
        Expr expr;
        if (match(SELECT)) {
            expr = selectSubQuery();
        } else {
            List<Expr> items = new ArrayList<>();
            do {
                expr = expression();
                items.add(expr);
            } while(match(COMMA));

            expr = new Expr.ListLiteral(items);
        }

        consume(RIGHT_PAREN, "Expect ')' after expression.");

        return expr;
    }

    private Expr selectSubQuery() {
        Expr.SelectSubQuery.Projection projection;
        if (match(STAR)) {
            projection = new Expr.SelectSubQuery.All();
        } else {
            List<Expr> columns = new ArrayList<>();
            do {
                var column = expression();
                columns.add(column);
            } while (match(COMMA));
            projection = new Expr.SelectSubQuery.Columns(columns);
        }

        consume(FROM, "Expect 'from' in select statement");
        var table = consume(IDENTIFIER, "Expect table name after 'from'");
        Expr clause = new Expr.AlwaysTrue();
        if (match(WHERE)) {
            clause = expression();
        }

        return new Expr.SelectSubQuery(projection, table, clause);
    }

    // -------------- Types ------- //
    private DBType dbtype() {
        if (match(INT)) return new DBType.Int();

        if (match(VARCHAR)) {
            consume(LEFT_PAREN, "Expect '(' after varchar.");
            int length = (int) consume(NUMBER, "Expect a number.").literal();
            consume(RIGHT_PAREN, "Expect ')'.");
            return new DBType.Varchar(length);
        }

        throw error("Unsupported data type.");
    }

    // ----------- Helper methods -------- //
    private Token advance() {
        if (!isAtEnd()) ++current;
        return prev();
    }

    private Token peek() {
        return tokens.get(current);
    }

    private Token prev() {
        return tokens.get(current - 1);
    }

    private boolean match(TokenType... expected) {
        for (var t : expected) {
            if (check(t)) {
                advance();
                return true;
            }
        }

        return false;
    }

    private Token consume(TokenType expected, String msg) {
        if (!check(expected)) {
            throw new SqlParseError(peek(), msg);
        }

        return advance();
    }

    private SqlParseError error(String msg) {
        return new SqlParseError(peek(), msg);
    }

    private boolean check(TokenType expected) {
        return peek().type() == expected;
    }

    private boolean isAtEnd() {
        return tokens.get(current).type() == EOF;
    }
}
