package edu.jdbc.lexical;

public record Token(TokenType type, String lexeme, Object literal, int line) {
}
