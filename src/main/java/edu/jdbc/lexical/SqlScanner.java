package edu.jdbc.lexical;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static edu.jdbc.lexical.TokenType.*;

public class SqlScanner {
    private static final Map<String, TokenType> KEYWORDS = new HashMap<>();

    static {
        KEYWORDS.put("create", CREATE);
        KEYWORDS.put("select", SELECT);
        KEYWORDS.put("insert", INSERT);
        KEYWORDS.put("delete", DELETE);
        KEYWORDS.put("drop", DROP);

        KEYWORDS.put("table", TABLE);
        KEYWORDS.put("database", DATABASE);
        KEYWORDS.put("from", FROM);
        KEYWORDS.put("into", INTO);
        KEYWORDS.put("set", SET);
        KEYWORDS.put("values", VALUES);
        KEYWORDS.put("int", INT);
        KEYWORDS.put("varchar", VARCHAR);
        KEYWORDS.put("update", UPDATE);

        KEYWORDS.put("where", WHERE);
        KEYWORDS.put("and", AND);
        KEYWORDS.put("in", IN);
        KEYWORDS.put("or", OR);
        KEYWORDS.put("not", NOT);
        KEYWORDS.put("true", TRUE);
        KEYWORDS.put("false", FALSE);
    }

    private final String source;
    private final List<Token> tokens;

    private int start;
    private int current;
    private int line;

    public SqlScanner(String source) {
        this.source = source;
        this.tokens = new ArrayList<>();
        this.line = 1;
    }

    public List<Token> scan() {
        start = current = 0;
        line = 1;

        while (!isAtEnd()) {
            start = current;
            char c = advance();
            switch (c) {
                case ',':
                    addToken(COMMA);
                    break;
                case ';':
                    addToken(SEMICOLON);
                    break;
                case '(':
                    addToken(LEFT_PAREN);
                    break;
                case ')':
                    addToken(RIGHT_PAREN);
                    break;
                case '=':
                    addToken(EQUAL);
                    break;
                case '*':
                    addToken(STAR);
                    break;
                case '-': {
                    if (match('-')) comment();
                    else addToken(MINUS);
                    break;
                }
                case '<': {
                    if (match('>')) addToken(NOT_EQUAL);
                    else if (match('=')) addToken(LESS_EQUAL);
                    else addToken(LESS);
                    break;
                }
                case '>': {
                    if (match('=')) addToken(GREATER_EQUAL);
                    else addToken(GREATER_EQUAL);
                    break;
                }
                case '.':
                    addToken(DOT);
                    break;
                case '\r':
                case '\t':
                case ' ':
                    break;
                case '\n':
                    ++line;
                    break;
                case '\'':
                    string();
                    break;
                default: {
                    if (isDigit(c)) {
                        number();
                    } else if (isAlphaNumeric(c)) {
                        identifierOrKeyword();
                    }
                }
            }
        }

        addToken(EOF);
        return tokens;
    }

    private void string() {
        while (!isAtEnd() && peek() != '\'') {
            if (peek() == '\n') {
                ++line;
            }
            advance();
        }

        advance();  // the closing '
        var s = source.substring(start + 1, current);
        addToken(STRING, s);
    }

    private void number() {
        while (!isAtEnd() && isDigit(peek())) {
            advance();
        }

        var num = Integer.parseInt(source.substring(start, current));
        addToken(NUMBER, num);
    }

    private void identifierOrKeyword() {
        while (!isAtEnd() && (isDigit(peek()) || isAlphaNumeric(peek()))) {
            advance();
        }

        var value = source.substring(start, current).toLowerCase(); // SQL is case-insensitive
        addToken(KEYWORDS.getOrDefault(value, IDENTIFIER));
    }

    private void comment() {
        while (!isAtEnd() && peek() != '\n') {
            advance();
        }
    }

    // ------------ Helper methods ------- //
    private void addToken(TokenType type) {
        addToken(type, null);
    }

    private void addToken(TokenType type, Object literal) {
        var token = new Token(type, source.substring(start, current).toLowerCase(), literal, line);
        tokens.add(token);
    }

    private boolean match(char expected) {
        if (peek() == expected) {
            advance();
            return true;
        }
        return false;
    }

    private char advance() {
        if (isAtEnd()) return '\0';
        return source.charAt(current++);
    }

    private char peek() {
        if (isAtEnd()) return '\0';
        return source.charAt(current);
    }

    private char peekNext() {
        if (current + 1 >= source.length()) return '\0';
        return source.charAt(current + 1);
    }

    private boolean isAtEnd() {
        return current >= source.length();
    }

    private boolean isDigit(char c) {
        return c >= '0' && c <= '9';
    }

    private boolean isAlphaNumeric(char c) {
        return (c >= 'a' && c <= 'z') ||
                (c >= 'A' && c <= 'Z') ||
                (c == '_');
    }
}
