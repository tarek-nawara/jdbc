package edu.jdbc;

import edu.jdbc.lexical.SqlScanner;
import edu.jdbc.parser.SqlParser;

public class JDBCMain {

    public static void main(String[] args) {
        String source = """
            -- This is a comment and should be ignored
            create table tarek (
                name varchar(100),
                age int
            );
            """;

        String subQuery = """
                select * from tarek
                where name in (select * from otherTable);
                """;
        var sqlScanner = new SqlScanner(subQuery);
        var tokens = sqlScanner.scan();

        var sqlParser = new SqlParser(tokens);
        var ast = sqlParser.parse();
        System.out.println(tokens);
        System.out.println(ast);
    }
}
