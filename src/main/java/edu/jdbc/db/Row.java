package edu.jdbc.db;

import java.util.Map;

public record Row(Map<String, Object> keyValues) {
}
